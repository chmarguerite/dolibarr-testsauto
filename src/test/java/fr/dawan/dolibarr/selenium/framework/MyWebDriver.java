package fr.dawan.dolibarr.selenium.framework;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

public class MyWebDriver extends ChromeDriver {
    int step=0;
    @Override
    public WebElement findElement(By by) {
        this.takeSnapShot("step" + step);
        step++;
        return by.findElement((SearchContext) this);
    }

    public  void takeSnapShot(String screenshot) {

        TakesScreenshot scrShot =((TakesScreenshot) this);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile=new File("target/surefire-reports/attachments/" + screenshot + ".jpeg");
        try {
            FileUtils.copyFile(SrcFile, DestFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
