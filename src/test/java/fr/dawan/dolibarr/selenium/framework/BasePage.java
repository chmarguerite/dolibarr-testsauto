package fr.dawan.dolibarr.selenium.framework;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class BasePage {
    WebDriver driver;
    public BasePage(){}

    public BasePage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public BasePage cliquerSurLeBouton(String button) throws Exception {
        getElement(button).click();
        return this;
    }
    public BasePage cliquerSurLeLien(String lien) throws Exception {
        getElement(lien).click();
        return this;
    }
    public BasePage cliquerSurLeMenu(String menu) throws Exception {
        getElement(menu).click();
        return this;
    }
    public BasePage saisirDansLeTexte(String texte,String valeur) throws Exception {
        getElement(texte).sendKeys(valeur);
        return this;
    }
    public BasePage sélectionnerDansLaListe(String liste,String valeur) throws Exception {
        new Select(getElement(liste)).selectByVisibleText(valeur);
        return this;
    }

    public WebElement getElement(String label) throws Exception {
        for (Field field : FieldUtils.getFieldsListWithAnnotation(this.getClass(),Name.class)) {
            Annotation annotation = field.getAnnotation(Name.class);
            if(annotation instanceof Name){
                if(((Name) annotation).label().equals(label)){
                    String getter = "get" + field.getName().substring(0,1).toUpperCase() + field.getName().substring(1);
                    return (WebElement) this.getClass().getDeclaredMethod(getter).invoke(this,null);
                }
            }
        }

        throw new Exception("Cet élement " + label + "n'existe pas");
    }


}
