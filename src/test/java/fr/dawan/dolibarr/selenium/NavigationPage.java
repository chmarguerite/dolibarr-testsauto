package fr.dawan.dolibarr.selenium;

import fr.dawan.dolibarr.selenium.framework.BasePage;
import fr.dawan.dolibarr.selenium.framework.Name;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class NavigationPage extends BasePage {

    @FindBy(id="mainmenua_companies")
    @Name(label = "Commerce")
    private WebElement lienCommerce;
    @FindBy(linkText="Nouveau tiers")
    @Name(label = "Nouveau tiers")
    private WebElement lienNouveauTiers;

    public NavigationPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

}
