package fr.dawan.dolibarr.selenium;

import fr.dawan.dolibarr.selenium.framework.BasePage;
import fr.dawan.dolibarr.selenium.framework.Name;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class NouveauTiersPage extends BasePage {

    @FindBy(id="name")
    @Name(label = "Nom client")
    private WebElement nomClient;
    @FindBy(id="customerprospect")
    @Name(label = "Type client")
    private WebElement listeProspect;
    @FindBy(id="fournisseur")
    @Name(label = "Fournisseur")
    private WebElement fournisseur;
    @FindBy(id="address")
    @Name(label = "Adresse")
    private WebElement adresse;
    @FindBy(name = "create")
    @Name(label = "Créer tiers")
    private WebElement créerTiers;

    public NouveauTiersPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }




}
