package fr.dawan.dolibarr.selenium;

import fr.dawan.dolibarr.selenium.framework.MyWebDriver;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.squashtest.ta.galaxia.tf.param.service.TFParamService;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.devicefarm.DeviceFarmClient;
import software.amazon.awssdk.services.devicefarm.model.CreateTestGridUrlRequest;
import software.amazon.awssdk.services.devicefarm.model.CreateTestGridUrlResponse;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;


public class TiersTest {
    static WebDriver browser;
    @BeforeClass
    public static void setupAll() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        browser = new RemoteWebDriver(new URL("http://35.180.230.243:4444/wd/hub"), capabilities);
        /*
        //capabilities.setCapability("video", "True");
        //browser= new RemoteWebDriver(new URL("https://W70iCKSLe3qxH0oVwJW3eqq9UM3SgGa2:D0a6BTHFMX3kOi5aRS4V8lVTXOB3lCWP@72gprh44-hub.gridlastic.com/wd/hub"),capabilities);
        String myProjectARN = "arn:aws:devicefarm:us-west-2:894249694327:testgrid-project:8ce45bdd-340f-4a32-847a-7dbf6f86a5d3";
        DeviceFarmClient client  = DeviceFarmClient.builder().region(Region.US_WEST_2).build();
        CreateTestGridUrlRequest request = CreateTestGridUrlRequest.builder()
                .expiresInSeconds(300)
                .projectArn(myProjectARN)
                .build();
        CreateTestGridUrlResponse response = client.createTestGridUrl(request);
        URL testGridUrl = new URL(response.url());
        System.out.println(testGridUrl);
        // You can now pass this URL into RemoteWebDriver.
        browser= new RemoteWebDriver(testGridUrl, capabilities);
        //System.setProperty("webdriver.chrome.driver","D:\\dev\\driver\\chromedriver.exe");
        browser = new MyWebDriver();
        browser.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        browser.manage().window().maximize();*/

    }
    String pNom;String pType; String pFournisseur;String pAdresse;
    @Test
    public void creationTiers() throws Exception {

        browser.get("http://dolibarr.testlogiciel.io/");

        new ConnexionPage(browser)
                .saisirDansLeTexte("Login","admin")
                .saisirDansLeTexte("Password","Dawan2020")
                .cliquerSurLeBouton("Connexion");

        new NavigationPage(browser)
                .cliquerSurLeMenu("Commerce")
                .cliquerSurLeLien("Nouveau tiers");

        initParameters();
        new NouveauTiersPage(browser)
                .saisirDansLeTexte("Nom client",pNom)
                .sélectionnerDansLaListe("Type client",pType)
                .sélectionnerDansLaListe("Fournisseur",pFournisseur)
                .saisirDansLeTexte("Adresse",pAdresse)
                .cliquerSurLeBouton("Créer tiers");

        takeSnapShot("Fiche_Tiers");
        assertTrue(browser.findElement(By.xpath("//span[text()='Ouvert']")).isDisplayed());

    }
    public void initParameters(){
        pNom=getValue("nom","Un nom de client");
        pType=getValue("type","Client");
        pFournisseur=getValue("fournisseur","Non");
        pAdresse=getValue("adresse","12 rue quelque part");

    }
    private String getValue(String parameter, String sinon){
        String tmParameter=TFParamService.getInstance().getParam("DS_" + parameter);
        return tmParameter!=null?tmParameter:sinon;
    }

    @AfterClass
    public static void closeAll(){

        //System.out.println("Test Video:https://s3-eu-west-1.amazonaws.com/be8f5d0a-c2d2-9383-27b0-464cabf83d80/ab66cc87-98a3-d43f-93e3-114e0b692687/play.html?" + ((RemoteWebDriver) browser).getSessionId());
        browser.close();
        browser.quit();
    }



    public static void takeSnapShot(String screenshot) throws Exception{

        TakesScreenshot scrShot =((TakesScreenshot) browser);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile=new File("target/surefire-reports/attachments/" + screenshot + ".jpeg");
        FileUtils.copyFile(SrcFile, DestFile);

    }
}
