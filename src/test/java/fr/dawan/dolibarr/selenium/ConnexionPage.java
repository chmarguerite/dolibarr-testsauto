package fr.dawan.dolibarr.selenium;

import fr.dawan.dolibarr.selenium.framework.BasePage;
import fr.dawan.dolibarr.selenium.framework.Name;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class ConnexionPage extends BasePage {


    @FindBy(id="username")
    @Name(label = "Login")
    private WebElement login;
    @FindBy(id="password")
    @Name(label = "Password")
    private WebElement password;
    @Name(label = "Connexion")
    @FindBy(className = "button")
    private WebElement connexion;

    public ConnexionPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }





















































}
